json.extract! card_page, :id, :title, :h1, :body, :cards, :created_at, :updated_at
json.url card_page_url(card_page, format: :json)
