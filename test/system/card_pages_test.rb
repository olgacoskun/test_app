require "application_system_test_case"

class CardPagesTest < ApplicationSystemTestCase
  setup do
    @card_page = card_pages(:one)
  end

  test "visiting the index" do
    visit card_pages_url
    assert_selector "h1", text: "Card Pages"
  end

  test "creating a Card page" do
    visit card_pages_url
    click_on "New Card Page"

    fill_in "Body", with: @card_page.body
    fill_in "Cards", with: @card_page.cards
    fill_in "H1", with: @card_page.h1
    fill_in "Title", with: @card_page.title
    click_on "Create Card page"

    assert_text "Card page was successfully created"
    click_on "Back"
  end

  test "updating a Card page" do
    visit card_pages_url
    click_on "Edit", match: :first

    fill_in "Body", with: @card_page.body
    fill_in "Cards", with: @card_page.cards
    fill_in "H1", with: @card_page.h1
    fill_in "Title", with: @card_page.title
    click_on "Update Card page"

    assert_text "Card page was successfully updated"
    click_on "Back"
  end

  test "destroying a Card page" do
    visit card_pages_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Card page was successfully destroyed"
  end
end
