require 'test_helper'

class CardPagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @card_page = card_pages(:one)
  end

  test "should get index" do
    get card_pages_url
    assert_response :success
  end

  test "should get new" do
    get new_card_page_url
    assert_response :success
  end

  test "should create card_page" do
    assert_difference('CardPage.count') do
      post card_pages_url, params: { card_page: { body: @card_page.body, cards: @card_page.cards, h1: @card_page.h1, title: @card_page.title } }
    end

    assert_redirected_to card_page_url(CardPage.last)
  end

  test "should show card_page" do
    get card_page_url(@card_page)
    assert_response :success
  end

  test "should get edit" do
    get edit_card_page_url(@card_page)
    assert_response :success
  end

  test "should update card_page" do
    patch card_page_url(@card_page), params: { card_page: { body: @card_page.body, cards: @card_page.cards, h1: @card_page.h1, title: @card_page.title } }
    assert_redirected_to card_page_url(@card_page)
  end

  test "should destroy card_page" do
    assert_difference('CardPage.count', -1) do
      delete card_page_url(@card_page)
    end

    assert_redirected_to card_pages_url
  end
end
