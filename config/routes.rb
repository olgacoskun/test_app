Rails.application.routes.draw do
  resources :cards
  resources :card_pages

  get '/mednews', to: 'mednews#mednews', as: :mednews
end
